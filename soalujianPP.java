import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.InputMismatchException;

class SoalNomor1 {	
	private int n;
	private int r;
	private int pilihan;
	
	SoalNomor1 (int n, int r, int pilihan){
		this.n = n;
		this.r = r;
		this.pilihan = pilihan;
	}	
    
	long faktorial(int n){
		long result=1;
		for (int indeks=n; indeks>1; indeks--){
			result*=indeks;
		}return result;
	}
    
	long permutasi(int n, int r){
		return faktorial(n)/faktorial(n-r);
	}
    
	float combinasi(int n, int r){
		return permutasi(n,r)/faktorial(r);
	}
    
	String run() throws IOException{		
		String result = "";	
		if (n < r){
			return ("Invalid input, Nilai N harus lebih besar dari R");
		}
		switch(pilihan){
		case 1:			
			result = ("Nilai Permutasi Variabel N dan Variabel R : "+ permutasi(n,r));
			break;
		case 2: 
			String combinasi = String.format("%.1f", combinasi(n,r));
			result = ("Nilai Combinasi Variabel N dan Variabel R : "+ combinasi);
			break;
		}
		return result;
	}
}

class SoalNomor2 {	
	private int a,b,c;
	SoalNomor2 (int a, int b, int c){
		this.a= a;
		this.b= b;
		this.c= c;
	}
	String titikBalik(){
		float x= (-b)/(2*a);
		float y= (b*b-(4*a*c))/((-4)*a);
		String x1 = String.format("%.1f", x);
		String y1 = String.format("%.1f", y);
		String result="Titik balik ( "+x1+", "+y1+" )";
		return result;
	}
	String checkBentukGrafik(){
		String result = null;
		if (a > 0){
			result = "Grafik Terbuka Ke Atas, Titik Balik Minimum";
		}else{
			result = "Grafik Terbuka Ke Bawah, Titik Balik Maksimum";
		}
		return result;
	}
	String checkPosisiTitikBalik(){
		String result = null;
		float x = (-b)/(2*a);
		if (x ==0){
			result = "Titik Balik Terletak di Sumbu Y";
		}else if (x < 0){
			result = "Titik Balik Disebelah Kiri Sumbu Y";
		}else if (x > 0){
			result = "Titik Balik Disebelah Kanan Sumbu Y";
		}
		return result;
	}
	String checkSinggungY(){
		String result = null;
		if (c == 0){
			result = "Grafik Melalui Titik (0 , 0)";
		}else if ( c > 0){
			result = "Grafik Memotong Sumbu Y diatas Sumbu X ";
		}else if ( c < 0){
			result = "Grafik Memotong Sumbu Y dibawah Sumbu X";
		}
		return result;
	}
}
class soalujianPP{
	static byte pilihan ;
	static short n , r ;
	static short variabel_a;
	static short variabel_b;
	static short variabel_c;	
	static String resetStatus = "";
	static String executeStatus = "";
	static boolean errorFound_nomor1 = false;
	static boolean errorFound_nomor2 = false;
	
	static void readFile() throws IOException {
		FileReader file = new FileReader("file_input.txt");
		BufferedReader fileReader = new BufferedReader(file);
		for (int indeks= 1; indeks <= 10; indeks++){
			String contents =  fileReader.readLine();
			try{
				if (indeks == 2){
					pilihan = Byte.parseByte(contents);
					if (pilihan != 1 && pilihan != 2){
						 errorFound_nomor1 = true;
					}
				}else if (indeks == 3){
					n = Short.parseShort(contents);
				}else if (indeks == 4){
					r = Short.parseShort(contents);
				}
			}catch (NumberFormatException e){
				errorFound_nomor1 = true;
			}
			
			try{
				if (indeks == 8){
					variabel_a = Short.parseShort(contents);
				}else if (indeks ==9){
					variabel_b = Short.parseShort(contents);
				}else if (indeks == 10){
					variabel_c = Short.parseShort(contents);
				}
			}catch (NumberFormatException e){
				errorFound_nomor2 = true;
			}
		}
		fileReader.close();
		file.close();		
	}
	
	static void makeHowToUse()throws IOException{
		FileWriter file = new FileWriter( "How to Use.txt" );
		PrintWriter outFile = new PrintWriter(file);
		outFile.println("Cara pakai aplikasi:");
		outFile.println("1. buka aplikasi , tekan \"1\" untuk membuat fileinput.txt dan fileoutput.txt");
		outFile.println("2. masukan input dengan mengubah isi fileinput.txt sesuai degan ketentuan aplikasi");
		outFile.println("3. kembali ke aplikasi, tekan \"2\" untuk memproses");
		outFile.println("4. output program akan ditampilkan dalam fileoutput.txt");
		outFile.println("\n\n");
		outFile.println("Terima Kasih :)");
		outFile.close();
		file.close();
	}
	static void makeFileInput()throws IOException{
		FileWriter file = new FileWriter( "file_input.txt" );
		PrintWriter outFile = new PrintWriter(file);
		outFile.println("1. Input untuk soal Nomor 1");
		outFile.println("#Masukkan pilihan anda");
		outFile.println("#n");
		outFile.println("#r");
		outFile.println("");
		outFile.println("");
		outFile.println("2. Input untuk soal Nomor 2");
		outFile.println("#variabel_a");
		outFile.println("#variabel_b");
		outFile.println("#variabel_c");
		outFile.println("");
		outFile.println("");
		outFile.println("");
		outFile.println("");
		outFile.println("//keterangan:");
		outFile.println("// ubah \"#..\" dengan angka yang diinginkan sesuai dengan batasan yang ditentukan");
		outFile.println("// Untuk memahami inputan soal nomor 1 buka fileoutput.txt untuk melihat Menu pilihan");
		outFile.println("contoh ==> 	\"#Masukkan pilihan anda\" ubah menjadi \"2\"");
		outFile.println("contoh untuk input soal nomor 1 (pada baris ke-2, ke-3, dan ke-4 diatas)");
		outFile.println("/*");
		outFile.println("\t1. Input untuk soal Nomor 1");
		outFile.println("\t2");
		outFile.println("\t6");
		outFile.println("\t3");
		outFile.println("*/");
		outFile.println("//Peringatan:");
		outFile.println("//Jangan mengubah isi file ini selain yang diawali dengan tanda \"#\"");
		outFile.println("//jangan menambahkan spasi dan garis baru yang tidak perlu");
		outFile.close();
		file.close();
	}
	static void makeFileOutput() throws IOException{
		FileWriter file = new FileWriter("fileoutput.txt" );
		PrintWriter outFile = new PrintWriter(file);
		outFile.println("1. Output untuk soal nomor 1");
		outFile.println("\tMenu");
		outFile.println("1. Permutasi");
		outFile.println("2. Combinasi");
		outFile.println("Tentukan pilihan = #readFrom.file_input.txt");
		outFile.println("Nilai variabel N : #readFrom.file_input.txt");
		outFile.println("Nilai variabel R : #readFrom.file_input.txt");
		outFile.println("===outputProgram===");
		outFile.println();
		outFile.println();
		outFile.println("2. Output untuk soal nomor 2");
		outFile.println("Nilai variabel a = #readFrom.file_input.txt");
		outFile.println("Nilai variabel b = #readFrom.file_input.txt");
		outFile.println("Nilai variabel c = #readFrom.file_input.txt");
		outFile.println("\tax^2 + bx + c");
		outFile.println("===outputProgram===");		
		outFile.close();
		file.close();
	}
	static void execute() throws IOException{
		FileWriter file = new FileWriter("file_output.txt" );
		PrintWriter outFile = new PrintWriter(file);
		SoalNomor1 nomor1 = new SoalNomor1(n, r, pilihan);
		outFile.println("*Output untuk soal nomor 1");		
		if (errorFound_nomor1){
			outFile.println();
			outFile.println("#Output program not display");
			outFile.println("#Invalid input, check file_input.txt correctly");			
		}else {			
			outFile.println("\tMenu");
			outFile.println("1. Permutasi");
			outFile.println("2. Combinasi");outFile.println("Tentukan pilihan = "+ pilihan);
			outFile.println("Nilai variabel N : "+ n);
			outFile.println("Nilai variabel R : "+ r);
			outFile.println(nomor1.run());
		}	
		outFile.println();
		outFile.println();
		outFile.println();
		outFile.println();
		SoalNomor2 nomor2 = new SoalNomor2(variabel_a, variabel_b, variabel_c);
		outFile.println("*Output untuk soal nomor 2");
		if (errorFound_nomor2){
			outFile.println();
			outFile.println("#Output program not display");
			outFile.println("#Invalid input, check file_input.txt correctly");
		}else{
			outFile.println("Nilai variabel a = "+ variabel_a);
			outFile.println("Nilai variabel b = "+ variabel_b);
			outFile.println("Nilai variabel c = "+ variabel_c);
			outFile.println("\t"+ variabel_a +"x^2 + "+ variabel_b +"x + "+ variabel_c);
			outFile.println(nomor2.titikBalik());
			outFile.println(nomor2.checkBentukGrafik());
			outFile.println(nomor2.checkPosisiTitikBalik());
			outFile.println(nomor2.checkSinggungY());
		}	
		outFile.close();
		file.close();
	}
	static void menu(){
		System.out.println();
		System.out.println("\t\t\t Selamat Datang");
		System.out.println("1. Buat/atur ulang file input dan file output" + "\t\t\t\t" + resetStatus);
		System.out.println("2. Eksekusi                                " + "\t\t\t\t" + executeStatus);								
		System.out.println("0. Keluar dari Aplikasi");		
	}
	static int input() throws IOException{
		int result=0;
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		boolean reInput= false;
		do{
			String testInput= input.readLine();
			try{
				result= Integer.parseInt(testInput);
				reInput=false;
			}catch(InputMismatchException e){
				System.out.print("Incorrect input! try input correctly = ");
				reInput=true;
			}catch(NumberFormatException e){
				System.out.print("Incorrect input! try input correctly = ");
				reInput=true;
			}
		}while(reInput);
		return result;
	}
	
	public static void main(String[]args)throws IOException{
		boolean repeat = true;
		do{
			menu();
			boolean selectedIncorrect = false;
			do{
				System.out.print("MASUKKAN PILIHAN ANDA : ");		
				int choice = input();
				switch (choice){
				case 1:
					makeFileInput();
					makeFileOutput();
					makeHowToUse();
					executeStatus = "";
					resetStatus = "SELESAI";
					selectedIncorrect = false;
					break;
				case 2:
					readFile();
					execute();
					executeStatus = "SELESAI";
					selectedIncorrect = false;
					break;
				case 0:
					repeat = false;
					selectedIncorrect = false;
					break;
				default:
					selectedIncorrect = true;
				}
			}while (selectedIncorrect);
			
		}while (repeat);
		
	}
}
