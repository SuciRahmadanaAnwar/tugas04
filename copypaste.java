import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.File;
public class copypaste {
	public static void main (String[] args){
		String CopyFile = "CopyFile.txt";
		String PasteFile = "PasteFile.txt";
		String contentCopyFile = null;
		String contentPasteFile = null;
		File file = new File(PasteFile);
		
		try{
			FileReader fileReader = new FileReader(CopyFile);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			while((contentCopyFile = bufferedReader.readLine()) != null){
				contentPasteFile = contentCopyFile;
				if(!file.exists()){
					file.createNewFile();
				}
				bufferedWriter.write(contentPasteFile);
				bufferedWriter.newLine();
				System.out.println(contentPasteFile);
			}
			bufferedReader.close();
			bufferedWriter.close();
		}
		catch(FileNotFoundException e){
			System.out.println("File "+CopyFile+" tidak ditemukan");
			
		}
		catch(IOException e){
			System.out.println("File "+CopyFile+" tidak dapat dibaca");
		}
		catch(Exception e){
			System.out.println(e);
		}
	}
}
