import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.File;
public class pascalTriangle{
	public static void main (String[] args){
		System.out.println("===========Segitiga Pascal===========");
		try{
			String fileHigh = "tinggiPascal.txt";
			FileReader fileReader = new FileReader(fileHigh);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String high = null;
			high = bufferedReader.readLine();
			int highTriangle = Integer.parseInt(high);
			
			String resultpascalTriangle = "hasilSegitigaPascal.txt";
			File fileResult = new File(resultpascalTriangle);
			FileWriter fileWriter = new FileWriter(fileResult.getAbsoluteFile());
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			
			for (int indeksA=0; indeksA<highTriangle ; indeksA++){
				for (int indeksB=1; indeksB<=(highTriangle-indeksA)*2; indeksB++){
					System.out.print(" ");
					bufferedWriter.write(" ");
				}
				int number = 1;
				for (int indeksC = 0; indeksC<=indeksA; indeksC++){
					System.out.print(number+"   ");
					bufferedWriter.write(number+"   ");
					number = number * (indeksA-indeksC)/(indeksC+1);
				}
				System.out.println();
				bufferedWriter.newLine();
			}
			bufferedReader.close();
			bufferedWriter.close();
		}
		catch(FileNotFoundException e){
			System.out.println("File tidak ditemukan");	
		}
		catch(IOException e){
			System.out.println("File tidak dapat dibaca");
		
		}	
	}
}