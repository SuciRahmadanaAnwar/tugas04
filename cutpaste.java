import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.File;
public class cutpaste {
	public static void main (String[] args){
		String CutFile = "CutFile.txt";
		String PasteFile = "PasteFile.txt";
		String contentCutFile = null;
		String contentPasteFile = null;
		File afile = new File(CutFile);
		File bfile = new File(PasteFile);
		
		try{
			FileReader fileReader = new FileReader(CutFile);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			FileWriter fileWriter = new FileWriter(bfile.getAbsoluteFile());
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			while((contentCutFile = bufferedReader.readLine()) != null){
				contentPasteFile = contentCutFile;
				if(!bfile.exists()){
					bfile.createNewFile();
				}
				bufferedWriter.write(contentPasteFile);
				bufferedWriter.newLine();
				System.out.println(contentPasteFile);
			}
			bufferedReader.close();
			bufferedWriter.close();
			
			afile.deleteOnExit();
		}
		catch(FileNotFoundException e){
			System.out.println("File tidak ditemukan");
			
		}
		catch(IOException e){
			System.out.println("File tidak dapat dibaca");
		}
		catch(Exception e){
			System.out.println(e);
		}
	}
}
